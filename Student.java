public class Student {
    double gradeFirst;
    double gradeSecond;
    double gradeThird;

    public Student(double gradeFirst, double gradeSecond, double gradeThird) {
        this.gradeFirst = gradeFirst;
        this.gradeSecond = gradeSecond;
        this.gradeThird = gradeThird;
    }

    public Student(Student s) {
        this(s.gradeFirst, s.gradeSecond, s.gradeThird);
    }

    public void inPlaceSort() {
        helperSort(this);
    }

    public Student outPlaceSort() {
        Student aux = new Student(this);
        helperSort(aux);
        return aux;
    }

    @Override
    public String toString() {
        return this.gradeFirst + " " + this.gradeSecond + " " + this.gradeThird;
    }

    private void helperSort(Student s) {
        double[] fieldArray = new double[]{s.gradeFirst, s.gradeSecond, s.gradeThird};
        double temp;
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = i + 1; j < fieldArray.length ; j++) {
                if (fieldArray[j] < fieldArray[i]) {
                    temp = fieldArray[i];
                    fieldArray[i] = fieldArray[j];
                    fieldArray[j] = temp;
                }
            }
        }
        s.gradeFirst = fieldArray[0];
        s.gradeSecond = fieldArray[1];
        s.gradeThird = fieldArray[2];

    }


}