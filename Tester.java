public class Tester {
    public static void main(String[] args) {
        Student student = new Student(34.1, 16.8, 22.8);

        System.out.println("Student's grades: "+student.toString());

        student.inPlaceSort();

        System.out.println("After in-place sorting: "+student.toString());

        Student sortedStudent = student.outPlaceSort();

        System.out.println("After out-of-place sorting: "+sortedStudent.toString());

        System.out.println(student == sortedStudent);

    }
}

